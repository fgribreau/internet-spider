const axios = require('axios');

module.exports = (elasticSearch_endpoint, elasticSearch_index) => {


    /**
     *
     * @param {String} url
     * @param {String} content
     * @return {Promise<ElasticearchResponse>}
     */
    return function indexPage(url, content) {
        console.debug(`Indexing ${url}...`);

        return axios({
            method: 'PUT',
            url: `${elasticSearch_endpoint}/${elasticSearch_index}/_doc/${encodeURIComponent(url)}`,
            data: {
                url,
                content
            }
        }).catch(err => {
            console.log(err);
        })
    };
};
