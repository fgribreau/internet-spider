# Internet-Spider

**Work-in-progress** live-coding at EPSI-Nantes.

## Setup

```bash
npm install
```

## Start

```bash
source .env
npm start
```

or

```bash
ELASTICSEARCH_ENDPOINT='http://localhost:9200' npm start
```

See [12 factors](https://12factor.net/)

## Tests

```bash
npm test
```
