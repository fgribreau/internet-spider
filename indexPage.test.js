const config = require('./config');
const indexPage = require('./indexPage')(config.elasticsearch.endpoint, config.elasticsearch.index);
const axios = require('axios');

describe('indexPage', function () {
    it('works', () => {
        const url = 'http://plop.com';
        return indexPage(url, '<p>Hello world</p>').then(() =>

            axios(`${config.elasticsearch.endpoint}/${config.elasticsearch.index}/${url}`).then(response =>expect(response.data).toMatchInlineSnapshot())
        );
    })
});
