const config = require('./config');
const downloadPage = require('./downloadPage');
const extractUrls = require('./extractUrls');
const indexPage = require('./indexPage')(config.elasticsearch.endpoint);

function ScrapeQueue(initialValue = []) {
    let _list = initialValue;

    function add(url) {
        console.debug(`Trying to add ${url} to the queue`);
        const set = new Set(_list);
        set.add(url);
        _list = [...set];
    }

    function addAll(urls) {
        urls.forEach(add);
    }

    /**
     * Yield the first element of the queue
     */
    const shift = () =>
        _list.shift();


    return {
        shift,
        add,
        addAll
    }
}

const queue = new ScrapeQueue();

/**
 * @param {Array} queue
 * @return {Promise<T???>}
 */
function scrapeURL(queue) {
    const url = queue.shift(); // take the queue first element

    console.debug(`Scraping url ${url}`);
    return downloadPage(url).then(html => {
        return {index: indexPage(url, html), html};
    }).then(({html}) => {
        queue.addAll(extractUrls(html));
    });
}

queue.add(config.scraper.startUrl);

function loop() {
    return scrapeURL(queue).then(_ => scrapeURL(queue));
}

return loop().then(() => console.log(`DONE ! I've indexed the web \o/`));


// START_URL=https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:Accueil_principal npm start
