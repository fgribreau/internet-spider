const downloadPage = require('./downloadPage');
describe('downloadPage', () => {
    it('should work', () =>
        downloadPage('https://www.google.com')
            .then(html => typeof html === 'string')
    );

    it('yield a getaddrinfo error if the page does not exist', () =>
        downloadPage('http://plopoooooooooooooqsoijqs.sdoiujsd').catch(err => expect(err.code).toBe('ENOTFOUND'))
    );
});
