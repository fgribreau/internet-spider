const urlRegex = require('url-regex');
const URL_REGEX = urlRegex();
const Url = require('url');

/**
 *
 * @param {string} html
 * @return {Array<String>}
 */
function extractUrls(html) {
    return [...new Set([...html.matchAll(URL_REGEX)].map(([url]) => {
        const urlObject = Url.parse(url);
        /**
         Url {
              protocol: 'https:',
              slashes: true,
              auth: null,
              host: 'www.wikimedia.com',
              port: null,
              hostname: 'www.wikimedia.com',
              hash: null,
              search: null,
              query: null,
              pathname: '/',
              path: '/',
              href: 'https://www.wikimedia.com/'
            }
         */
        if(!urlObject.protocol){
            urlObject.protocol = 'https';
        }

        urlObject.query = null;
        urlObject.search = null;

        return Url.format(urlObject);
    }))];
}

module.exports = extractUrls;
