const env = require('common-env/withLogger')(console);
module.exports = env.getOrElseAll({
   elasticsearch:{
       endpoint: { // 'http://localhost:9200'
           $type: env.types.String
       },
       index: 'internet'
   },
    scraper:{
       startUrl:'https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:Accueil_principal'
    }
});
