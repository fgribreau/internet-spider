const axios = require('axios');
/**
 *
 * @param {String} url
 * @return {Promise<HTML>}
 */
const downloadPage = (url) => axios(url).then(response =>
    response.data
);


module.exports = downloadPage;
